# Test tècnic Paymefy

Requereix php 7.1.3 o superior.

Executar `composer install` per instal·lar vendors.

L'aplicació està implementada amb Symfony 4.3. La forma més fàcil d'executar-la és a través del servidor web que Symfony proveeix (https://symfony.com/download).

Executar `symfony server:start` per arrancar el servidor.


Per configurar el número d'ascensors editar *config/services.yaml* 

```
parameters:
    app.number_elevators: 3
```

Les seqüències es configuren a *config/sequences.yaml*, exemple:

```
sequences:
  - interval: 5
    start: 9:00
    end: 11:00
    call: [0]
    destination: [1, 2]
```

on:
 - interval són els minuts
 - start i end són l'hora d'inici i final en format 24 hores
 - call és un array amb els números de planta des de les que és fan les crides
 - destination és un array amb els números de planta destinació
 
 El número de plantes es configura a les seqüencies:
 
 ```
 sequences:
   - interval: 5
     ...
     call: [4]
     destination: [5, 6]
 ```

