<?php

namespace App\Controller;

use App\Sequence\Generator;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ReportController extends AbstractController
{
    /**
     * @param Generator $generator
     * @return Response
     * @throws Exception
     *
     * @Route("/", name="report")
     */
    public function index(Generator $generator)
    {
        return $this->render('report/index.html.twig', ['generator' => $generator]);
    }
}
