<?php
/**
 * Created by PhpStorm.
 * User: david.ramentol@gmail.com
 * Date: 12/11/2019
 * Time: 22:15
 */

namespace App\Sequence;


use App\Elevator\Controller;
use App\Model\Cell;
use App\Model\Request;
use App\Model\Row;
use DateInterval;
use DateTime;
use DateTimeImmutable;
use Exception;
use IteratorAggregate;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\Yaml\Yaml;
use Traversable;

/**
 * Class Generator
 * @package App\Sequence
 *
 *
 */
class Generator implements IteratorAggregate
{
    /**
     * @var Controller
     */
    private $controller;

    /**
     * @var string
     */
    private $projectDir;

    /**
     * Generator constructor.
     * @param Controller $controller
     * @param string $projectDir
     */
    public function __construct(Controller $controller, string $projectDir)
    {
        $this->controller = $controller;
        $this->projectDir = $projectDir;
    }

    /**
     * Retrieve an external iterator
     * @link https://php.net/manual/en/iteratoraggregate.getiterator.php
     * @return Traversable An instance of an object implementing <b>Iterator</b> or
     * <b>Traversable</b>
     * @since 5.0.0
     * @throws Exception
     */
    public function getIterator()
    {
        $requests = $this->generate();

        $oneMinute = DateInterval::createFromDateString('1 minute');

        for ($time = new DateTime('today 9:00'); $time <= new DateTime('today 20:00'); $time->add($oneMinute)) {

            $row = new Row();
            $row->setTime($time);

            foreach ($this->controller->getElevators() as $elevator) {
                $cell = new Cell($elevator->getId(), $elevator->getCurrentFloor(), $elevator->getFloorsTraveled());
                $row->addCell($cell);
            }

            yield $row;

            $currentRequests = array_filter($requests, function (Request $request) use ($time) {
                return $request->getTime() <= $time;
            });

            foreach ($currentRequests as $request) {
                if ($this->controller->call($request, DateTimeImmutable::createFromMutable($time))
                    && null !== ($key = array_search($request, $requests))) {
                    unset($requests[$key]);
                }
            }
        }
    }

    /**
     * @return Request[]
     * @throws Exception
     */
    private function generate(): array
    {
        /** @var Request[] $sequence */
        $sequence = [];
        $config = $this->parseConfigFile();

        foreach ($config['sequences'] as $item) {
            $this->processConfigItem($item, $sequence);
        }

        return $sequence;
    }

    private function parseConfigFile()
    {
        $fileLocator = new FileLocator([$this->projectDir . '/config']);
        $yamlSequenceFile = $fileLocator->locate('sequences.yaml', null, true);
        return Yaml::parseFile($yamlSequenceFile);
    }

    /**
     * @param array $config
     * @param array $sequence
     * @throws Exception
     */
    private function processConfigItem(array $config, array &$sequence)
    {
        $interval = DateInterval::createFromDateString($config['interval'] . ' minutes');

        $start = new DateTime('today ' . $config['start']);
        $end = new DateTime('today ' . $config['end']);

        $calls = $config['call'];
        $destinations = $config['destination'];

        for ($current = $start; $current <= $end; $current->add($interval)) {
            foreach ($calls as $call) {
                foreach ($destinations as $destination) {
                    $sequence[] = new Request(DateTimeImmutable::createFromMutable($current), $call, $destination);
                }
            }
        }
    }
}