<?php
/**
 * Created by PhpStorm.
 * User: david.ramentol@gmail.com
 * Date: 13/11/2019
 * Time: 10:05
 */

namespace App\Model;


use DateTimeInterface;

class Row
{
    /**
     * @var DateTimeInterface
     */
    private $time;

    /**
     * @var Cell[]
     */
    private $cells = [];

    /**
     * @return DateTimeInterface
     */
    public function getTime(): DateTimeInterface
    {
        return $this->time;
    }

    /**
     * @param DateTimeInterface $time
     */
    public function setTime(DateTimeInterface $time): void
    {
        $this->time = $time;
    }

    /**
     * @return array
     */
    public function getCells(): array
    {
        return $this->cells;
    }

    /**
     * @param array $cells
     */
    public function setCells(array $cells): void
    {
        $this->cells = $cells;
    }

    public function addCell(Cell $cell)
    {
        $this->cells[$cell->getId()] = $cell;
    }
}