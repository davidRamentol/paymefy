<?php
/**
 * Created by PhpStorm.
 * User: david.ramentol@gmail.com
 * Date: 11/11/2019
 * Time: 18:55
 */

namespace App\Model;


use DateTimeImmutable;

class Request
{
    /**
     * @var DateTimeImmutable
     */
    private $time;

    /**
     * @var int
     */
    private $callFloor;

    /**
     * @var int
     */
    private $destinationFloor;

    /**
     * Call constructor.
     * @param DateTimeImmutable $time
     * @param int[] $callFloor
     * @param int[] $destinationFloor
     */
    public function __construct(DateTimeImmutable $time, int $callFloor, int $destinationFloor)
    {
        $this->time = $time;
        $this->callFloor = $callFloor;
        $this->destinationFloor = $destinationFloor;
    }

    /**
     * @return DateTimeImmutable
     */
    public function getTime(): DateTimeImmutable
    {
        return $this->time;
    }

    /**
     * @return int
     */
    public function getCallFloor(): int
    {
        return $this->callFloor;
    }

    /**
     * @return int
     */
    public function getDestinationFloor(): int
    {
        return $this->destinationFloor;
    }
}