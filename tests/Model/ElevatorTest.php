<?php
/**
 * Created by PhpStorm.
 * User: david.ramentol@gmail.com
 * Date: 12/11/2019
 * Time: 21:58
 */

namespace App\Tests\Model;

use App\Model\Elevator;
use Symfony\Bundle\FrameworkBundle\Tests\TestCase;

class ElevatorTest extends TestCase
{
    public function testGoToFloor()
    {
        $elevator = new Elevator(1);

        $this->assertEquals(0, $elevator->getCurrentFloor());

        $elevator->goToFloor(3);
        $this->assertEquals(3, $elevator->getFloorsTraveled());

        $elevator->goToFloor(1);
        $this->assertEquals(5, $elevator->getFloorsTraveled());
    }

}
