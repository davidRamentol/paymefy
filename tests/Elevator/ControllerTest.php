<?php
/**
 * Created by PhpStorm.
 * User: david.ramentol@gmail.com
 * Date: 12/11/2019
 * Time: 15:44
 */

namespace App\Tests\Elevator;


use App\Elevator\Controller;
use App\Model\Elevator;
use App\Model\Request;
use DateTimeImmutable as DateTime;
use Symfony\Bundle\FrameworkBundle\Tests\TestCase;

class ControllerTest extends TestCase
{
    public function testCall()
    {
        $controller = new Controller(3);

        $elevator1 = $controller->call(new Request(new DateTime('today 09:00'), 0, 2));
        $elevator2 = $controller->call(new Request(new DateTime('today 09:00'), 0, 3));

        $controller->call(new Request(new DateTime('today 09:05'), 0, 2));
        $controller->call(new Request(new DateTime('today 09:05'), 0, 3));
        $controller->call(new Request(new DateTime('today 09:10'), 0, 2));
        $controller->call(new Request(new DateTime('today 09:10'), 0, 3));
        $controller->call(new Request(new DateTime('today 09:10'), 0, 1));
        $controller->call(new Request(new DateTime('today 09:10'), 0, 1));

        $this->assertInstanceOf(Elevator::class, $elevator1);
        $this->assertInstanceOf(Elevator::class, $elevator2);

        $this->assertNotSame($elevator1, $elevator2);
    }
}
